const HtmlWebPackPlugin=require('html-webpack-plugin')
module.exports={
    module:{
        rules:[
            {
                test:/\.(js|jsx)$/,
                exclude:/node_modules/,
                use:{

                    loader:'babel-loader'

                    //npm i babel-core babel-cli babel-preset-env babel-preset-react babel-loader
                },
                test:/.\css$/,
                use:['style-loader','css-loader'],
                //npm install css-loader style-loader

                test:/\.html$/,
                use:{
                    loader:'html-loader'
                }
            }
        ]
    },
    plugins:[
        new HtmlWebPackPlugin({
            template:'./index.html'
        })
    ]
}